# Atalhos

Definidos em [../lua/my-nvim/keymaps.lua] e [../lua/my-nvim/which-key.lua],
principalmente.

| tecla   | descrição            | modo |
|---------|----------------------|------|
| ]%      | next unmatched group | N    |
| [%      | prev unmatched group | N    |
| ](      | next (               | N    |
| [(      | prev (               | N    |
| ]<      | next <               | N    |
| [<      | prev <               | N    |
| ]{      | next {               | N    |
| [{      | prev {               | N    |
| ]q      | next quickfix        | N    |
| [q      | prev quickfix        | N    |
| ]d      | next diagnostic      | N    |
| [d      | prev diagnostic      | N    |
| ]m      | next method start    | N    |
| [m      | prev method start    | N    |
| ]M      | next method end      | N    |
| [M      | prev method end      | N    |
| ]h      | next hunk            | N    |
| [h      | prev hunk            | N    |
| \<C-q\> | quickfix toggle      | N    |
| \<S-l\> | next buffer          | N    |
| \<S-h\> | prev buffer          | N    |

## Movimentação entre janelas

| tecla   | descrição                     | modo |
|---------|-------------------------------|------|
| \<C-h\> | move para a janela à esquerda | N    |
| \<C-j\> | move para a janela abaixao    | N    |
| \<C-k\> | move para a janela acima      | N    |
| \<C-l\> | move para a janela à direita  | N    |

## Redimensionar janelas

| tecla       | descrição                     | modo |
|-------------|-------------------------------|------|
| \<C-Up\>    | reduz altura da janela        | N    |
| \<C-Down\>  | aumenta a altura da janela    | N    |
| \<C-Left\>  | reduz a largura da janela     | N    |
| \<C-Right\> | aumenta a largura da janela   | N    |

## Git

| tecla         | descrição         | modo |
|---------------|-------------------|------|
| \<leader\>ghj | next hunk         | N    |
| \<leader\>ghk | prev hunk         | N    |
| \<leader\>ghs | stage hunk        | N    |
| \<leader\>ghu | undo stage hunk   | N    |
| \<leader\>ghr | reset hunk        | N    |
| \<leader\>ghv | select hunk       | N    |
| \<leader\>ghp | preview hunk      | N    |
| \<leader\>gbs | stage buffer      | N    |
| \<leader\>gbu | undo stage buffer | N    |
| \<leader\>gbr | reset buffer      | N    |
| \<leader\>gbd | diff buffer       | N    |
| \<leader\>glb | blame line        | N    |

## Telescope

| tecla         | descrição           | modo |
|---------------|---------------------|------|
| \<leader\>tg  | live grep           | N    |
| \<leader\>tf  | find file           | N    |
| \<leader\>tb  | find buffer         | N    |
| \<leader\>td  | diagnostics         | N    |
| \<leader\>th  | help                | N    |
| \<leader\>tm  | manpages            | N    |
| \<leader\>tr  | resume last search  | N    |
| \<leader\>tGb | git buffer commits  | N    |
| \<leader\>tGc | git checkout commit | N    |
| \<leader\>tGo | open changed file   | N    |
| \<leader\>tGs | git stash           | N    |
| \<leader\>tGB | checkout branch     | N    |

## LSP

| tecla         | descrição           | modo |
|---------------|---------------------|------|
| gD            | goto declaration    | N    |
| gd            | goto definition     | N    |
| gi            | goto implementation | N    |
| gr            | goto references     | N    |
| \<leader\>lgD | goto declaration    | N    |
| \<leader\>lgd | goto definition     | N    |
| \<leader\>lgi | goto implementation | N    |
| \<leader\>lgr | goto references     | N    |
| \<leader\>la  | code action         | N    |
| \<leader\>lf  | format              | N    |
| \<leader\>lr  | rename              | N    |
| \<leader\>ls  | signature help      | N    |
| \<leader\>lo  | organize imports    | N    |
| \<leader\>lev | extract variable    | N    |
| \<leader\>lec | extract constant    | N    |
| \<leader\>lem | extract method      | V    |

## Diagnostics

| tecla        | descrição                        | modo |
|--------------|----------------------------------|------|
| \<leader\>df | Diagnostics in a floating window | N    |
| \<leader\>dq | Diagnostics to the quickfix list | N    |

## Trouble

| tecla        | descrição                          | modo |
|--------------|------------------------------------|------|
| \<leader\>xd | Buffer Diagnostics                 | N    |
| \<leader\>xl | Location List                      | N    |
| \<leader\>xL | LSP Definitions / references / ... | N    |
| \<leader\>xq | Quickfix List                      | N    |
| \<leader\>xs | Symbols                            | N    |
| \<leader\>xT | TODOs                              | N    |
| \<leader\>xx | Diagnostics                        | N    |

## TODO

| tecla        | descrição                   | modo |
|--------------|-----------------------------|------|
| \<leader\>Tq | Show TODOs in quickfix list | N    |
| \<leader\>Tx | Show TODOs in Trouble list  | N    |

## buffers

| tecla        | descrição            | modo |
|--------------|----------------------|------|
| \<leader\>bd | Buffer Delete        | N    |
| \<leader\>bD | Buffer Delete Others | N    |

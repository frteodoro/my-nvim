# Meu NeoVim: configurações que eu uso

Anotações sobre as minhas configurações do NeoVim, para eu conseguir refazer ou
compartilhar elas.

Usando muito das configurações do [LunarVim](https://www.lunarvim.org/), que eu
usava antes, como exemplos para estas configurações.

## Dependências

  * Neovim >= v0.10.0;

### Opcionais

  * node >= 16 (vários servidores LSP rodam em node:
    bash-language-server, css-lsp, eslint-lsp, html-lsp, json-lsp,
    typescript-language-server, yaml-language-server);
  * python3-venv (python3 com os módulos venv e ensurepip: necessário para o python-lsp-server);
  * ghcup (necessário para o haskell-language-server);


## Instalação

Basta baixar o conteúdo deste repositório (mil maneiras, invente uma) e
copiá-lo para `$XDG_CONFIG_HOME/$NVIM_APPNAME` (default: `~/.config/nvim`). Eu
costumo fazer usando `git clone` e tornar o `~/.config/nvim` um link simbólico
para este diretório clonado.

## Atalhos

A lista de atalhos definidos por esta configuração estão em
[doc/atalhos.md], configurados nos arquivos [./lua/my-nvim/keymaps.lua] e
[./lua/my-nvim/which-key.lua], principalmente.

### Configuração da tecla \<leader\>

A tecla Espaço é a mais fácil de acessar e não tem nenhum uso no modo Normal.

```lua
vim.g.mapleader = " "
```

### Keymappings

Alguns keymaps para facilitar a vida. Exemplos:

```lua
local opts = { noremap=true, silent=true }

-- Better window movement
vim.keymap.set('n', '<C-h>', '<C-w>h', opts)
vim.keymap.set('n', '<C-j>', '<C-w>j', opts)
vim.keymap.set('n', '<C-k>', '<C-w>k', opts)
vim.keymap.set('n', '<C-l>', '<C-w>l', opts)

-- Resize with arrows
vim.keymap.set('n', '<C-Up>', ':resize -2<CR>', opts)
vim.keymap.set('n', '<C-Down>', ':resize +2<CR>', opts)
vim.keymap.set('n', '<C-Left>', ':vertical resize -2<CR>', opts)
vim.keymap.set('n', '<C-Right>', ':vertical resize +2<CR>', opts)

-- QuickFix
function QuickFixToggle()
    for _, win in pairs(vim.fn.getwininfo()) do
        if win["quickfix"] == 1 then
            vim.cmd "cclose"
            return
        end
    end
    vim.cmd "copen"
end
vim.keymap.set('n', ']q', ':cnext<CR>', opts)
vim.keymap.set('n', '[q', ':cprev<CR>', opts)
vim.keymap.set('n', '<C-q>', ':lua QuickFixToggle()<CR>', opts)

-- Better indenting
vim.keymap.set('v', '<', '<gv', opts)
vim.keymap.set('v', '>', '>gv', opts)
```


## Configurações Básicas

Configuração de opções do NeoVim que não dependem de plugins. Contidas no
arquivo [./lua/my-nvim/options.lua].

### Yank to Clipboard

Configuração para o `y` copiar o conteúdo para o clipboard do sistema e o `p`
colar o conteúdo do clipboard do sistema para dentro do NeoVim.

```
vim.o.clipboard = "unnamedplus"
```

### Configurações de Indentação

Uso esta configuração que nunca vai bagunçar a formatação se o `tabstop` mudar:

```
vim.o.shiftwidth = 4   	-- indentação tem 4 espaços
vim.o.tabstop = 4      	-- tab tem 4 espaços
vim.o.expandtab = true 	-- converte tabs para espaços
```

### Número mínimo de linhas exibidas acima e abaixo do cursor

```
vim.o.scrolloff = 3
```

### Criar novos splits abaixo e a direita

```lua
vim.opt.splitbelow = true
vim.opt.splitright = true
```

### Cores

Usando o colorscheme moonfly ([./lua/my-nvim/colorscheme.lua]) e corrigindo
apenas o mínimo necessário para ter o texto legível. Por enquando optou-se pelo
mais simples possível.

#### Highlight Yanked Text

Para dar um retorno visual de uma área que foi copiada destacando o texto por
uma fração de segundos:

```
-- highlight yanked text
vim.api.nvim_create_autocmd('TextYankPost', {
  group = vim.api.nvim_create_augroup('highlight_yank', {}),
  desc = 'Hightlight selection on yank',
  pattern = '*',
  callback = function()
    vim.highlight.on_yank({higroup = 'Search', timeout = 200})
  end,
})
```

#### Destaque para a linha corrente

```
vim.o.cursorline = true
```

#### Números de Linhas

Eu uso a linha atual com o número da linha no arquivo em destaque e as demais
linhas com númeração relativa à linha atual.

```
vim.o.number = true
vim.o.relativenumber = true
```

#### Destaque de Espaços Extras

Destaca em vermelho os espaços no fim da linha, exceto quando estiver digitando
naquela linha.

```lua
vim.api.nvim_set_hl(0, 'ExtraWhiteSpace', {ctermbg='Red', bg='Red'})
vim.fn.matchadd('ExtraWhiteSpace', [[\s\+\%#\@<!$]])
```


## Plugins

Plugins que independem da linguagem de programação/markup sendo editada.

Várias configurações nesta sessão podem estar desatualizados, optei por
mantê-los aqui pelos exemplos de código, na dúvida veja o conteúdo de
[./lua/my-nvim/] que sempre será mais atualizado. Ver
[./lua/my-nvim/plugins.lua].

Pretendo ir atualizando aos poucos, se vir um erro favor abrir uma issue. Me
ajuda muito.

### [Packer](https://github.com/wbthomason/packer.nvim)

Plugin para instalar outros plugins.

Clonar o repositório:

```Shell
git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

Configuração de plugins:

```Lua
require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
end)
```

Comandos do Packer:

```Vim
-- You must run this or `PackerSync` whenever you make changes to your plugin configuration
-- Regenerate compiled loader file
:PackerCompile

-- Remove any disabled or unused plugins
:PackerClean

-- Clean, then install missing plugins
:PackerInstall

-- Clean, then update and install plugins
-- supports the `--preview` flag as an optional first argument to preview updates
:PackerUpdate

-- Perform `PackerUpdate` and then `PackerCompile`
-- supports the `--preview` flag as an optional first argument to preview updates
:PackerSync

-- Loads opt plugin immediately
:PackerLoad completion-nvim ale
```

### nvim-lspconfig

Facilita a configuração para o LSP Client do NeoVim.

Instalado via Packer:

```Lua
require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  use 'neovim/nvim-lspconfig'   -- Configurations for Nvim LSP
end)
```

Configuração de keybindings para LSP:

```Lua
-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<leader>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<leader>f', function() vim.lsp.buf.format { async = true } end, bufopts)
end

-- Abaixo, alguns exemplos de configurações de servidores LSP:
--local lsp_flags = {
--  -- This is the default in Nvim 0.7+
--  debounce_text_changes = 150,
--}
--require('lspconfig')['pyright'].setup{
--    on_attach = on_attach,
--    flags = lsp_flags,
--}
--require('lspconfig')['tsserver'].setup{
--    on_attach = on_attach,
--    flags = lsp_flags,
--}
--require('lspconfig')['rust_analyzer'].setup{
--    on_attach = on_attach,
--    flags = lsp_flags,
--    -- Server-specific settings...
--    settings = {
--      ["rust-analyzer"] = {}
--    }
--}
```

Em caso de problemas veja 
[Troubleshooting nvim-lspconfig](https://github.com/neovim/nvim-lspconfig#troubleshooting).

### [mason.nvim](https://github.com/williamboman/mason.nvim)

A configuração dos servidores LSP foi muito simplificada pelo uso deste plugin.
Ver as [configurações no diretório lsp](./lua/my-nvim/lsp/).


```lua
  -- LSP
  use 'neovim/nvim-lspconfig'
  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'
```

### [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)

Configurações e funcionalidades básicas para Treesitter. Treesitter cria uma
representação em árvore sintática do texto sendo editado.

Para instalar adicionar o seguinte à lista do Packer:
```lua
  use {
      'nvim-treesitter/nvim-treesitter',
      run = function()
          local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
          ts_update()
      end,
  }
```
E execute `nvim +PackerSync`.

Instalar também o tree-sitter CLI:
```shell
pacman -S tree-sitter
```

Configuração (exemplo):
```lua
require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all"
  --ensure_installed = { "c", "lua", "rust" },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  --sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  -- List of parsers to ignore installing (for "all")
  --ignore_install = { "javascript" },

  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
  -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

  highlight = {
    -- `false` will disable the whole extension
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    --disable = { "c", "rust" },
    -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
    --disable = function(lang, buf)
    --    local max_filesize = 100 * 1024 -- 100 KB
    --    local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
    --    if ok and stats and stats.size > max_filesize then
    --        return true
    --    end
    --end,

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}
vim.o.foldmethod = "expr"                         -- for treesitter based folding
vim.o.foldexpr = "nvim_treesitter#foldexpr()"     -- for treesitter based folding
vim.o.foldenable = true                           -- enable folding at startup.
vim.o.foldlevel = 4
```

### [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim)

Fuzzy finder de arquivos e várias coisas.

Packer:
```lua
use {
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.0',

    -- If `ensure_dependencies` is true, the plugins specified in `requires` will
    -- be installed.
    requires = {
      {'nvim-lua/plenary.nvim'},
      {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
    }
  }
}
```

Configs:
```lua
require'telescope'.setup{
  defaults = {
    --path_display = {shorten = {exclude = {-3, -2, -1}}},
    path_display = {"smart"},
    color_devicons = true,
  },
}
```

Keymaps:
```lua
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
```

E use `:checkhealth telescope` para verificar se foi tudo instalado
corretamente.

Veja também `:help telescope` para entender melhor como funciona o Telescope.

#### Dependencias

##### plenary

Biblioteca de funções Lua. É uma dependência do Telescope. Com o Packer, a
instrução de instalação desta dependência fica na opção `requires` do
Telescope.

##### [ripgrep](https://github.com/BurntSushi/ripgrep)

Um grep que respeita o gitignore e ignora arquivos ocultos e binários.

```shell
pacman -S ripgrep
```

##### [telescope-fzf-native](https://github.com/nvim-telescope/telescope-fzf-native.nvim)

Um fuzzy finder compilado para melhorar a performance. Instrução de instalação
desta dependência na opção `requires` do Telescope.

##### [fd](https://github.com/sharkdp/fd)

Um `find` mais simples;

```shell
pacman -S fd
```

##### [devicons](https://github.com/kyazdani42/nvim-web-devicons)

Ícones para os diferentes tipos de arquivos.

Packer:
```lua
use 'nvim-tree/nvim-web-devicons'
```

Configuração:
```lua
require'nvim-web-devicons'.setup {
  color_icons = true;
  default = true;
}
```

### Which Key

Ajuda muito a lembrar as keybindings.

Packer:
```lua
use {
  "folke/which-key.nvim",
  config = function()
    require("which-key").setup {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
  end
}
```

Mappings config:
```lua
-- Adiconados labels a keymaps já existentes apenas.
local wk = require("which-key")
wk.register({
  f = {
    name = "+find",
    b = {"Find Buffer"},
    f = {"Find File"},
    g = {"Live Grep"},
    h = {"Help Tags"},
  },
  e = {"Diagnostics in a Floating Window"},
  q = {"Diagnostics to the Location List"},
}, { prefix = "<leader>" })
```

### [gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim)

Mostra as linhas alteradas e tem comandos para adicionar mudanças ao stage do
git, e mais vários comandos relacionados ao git.

Packer:
```lua
use {
  'lewis6991/gitsigns.nvim',
  config = function()
    require('gitsigns').setup()
  end
}
```

Para deixar fixo o espaço dos sinais na janela:
```lua
vim.opt.signcolumn = "yes"
```

Por enquando não vou usar nenhuma keymaps para este plugin, mas existe um
[exemplo sugerido](https://github.com/lewis6991/gitsigns.nvim#keymaps) no site
do projeto. Pode ser que eu use no futuro.

### [nvim-cmp](https://github.com/hrsh7th/nvim-cmp)

Auto-complete. Além da navegação no código, os servidores LSP vão ser usados
para a funcionalidade de auto-completar com este plugin.

Packer:
```lua
-- cmp plugins
use "hrsh7th/nvim-cmp"        -- The completion plugin
use "hrsh7th/cmp-buffer"      -- buffer completions
use "hrsh7th/cmp-path"        -- path completions
use "hrsh7th/cmp-nvim-lsp"    -- LSP completions
use "hrsh7th/cmp-nvim-lua"    -- completions for neovim Lua API
use "saadparwaiz1/cmp_luasnip"

-- snippets
use "L3MON4D3/LuaSnip"
```

Config:
```lua
--   פּ ﯟ   some other good icons
local kind_icons = {
  Text = "",
  Method = "m",
  Function = "",
  Constructor = "",
  Field = "",
  Variable = "",
  Class = "",
  Interface = "",
  Module = "",
  Property = "",
  Unit = "",
  Value = "",
  Enum = "",
  Keyword = "",
  Snippet = "",
  Color = "",
  File = "",
  Reference = "",
  Folder = "",
  EnumMember = "",
  Constant = "",
  Struct = "",
  Event = "",
  Operator = "",
  TypeParameter = "",
}
-- find more here: https://www.nerdfonts.com/cheat-sheet

local cmp = require'cmp'
cmp.setup {
  -- REQUIRED - you must specify a snippet engine
  snippet = {
    expand = function(args)
      require'luasnip'.lsp_expand(args.body)
    end,
  },
  mapping = {
    ["<C-k>"] = cmp.mapping.select_prev_item(),
    ["<C-j>"] = cmp.mapping.select_next_item(),
    ["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-1), { "i", "c" }),
    ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(1), { "i", "c" }),
    ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),

    -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
    ["<C-y>"] = cmp.config.disable,

    ["<C-e>"] = cmp.mapping {
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    },

    -- Accept currently selected item. If none selected, `select` first item.
    -- Set `select` to `false` to only confirm explicitly selected items.
    ["<CR>"] = cmp.mapping.confirm { select = true },

    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
  },
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, vim_item)
      -- Kind icons
      vim_item.kind = string.format("%s", kind_icons[vim_item.kind])

      -- This concatenates the icons with the name of the item kind
      -- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) 

      vim_item.menu = ({
        nvim_lsp = "[LSP]",
        nvim_lua = "[NVim Lua]",
        luasnip = "[LuaSnip]",
        buffer = "[Buffer]",
        path = "[Path]",
      })[entry.source.name]
      return vim_item
    end,
  },
  sources = {
    { name = "nvim_lsp" },
    { name = "nvim_lua" },
    { name = "luasnip" },
    { name = "buffer" },
    { name = "path" },
  },
  window = {
    documentation = {
      border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
    },
  },
  confirm_opts = {
    behavior = cmp.ConfirmBehavior.Replace,
    select = false,
  },
}
```

### [nvim-tree](https://github.com/nvim-tree/nvim-tree.lua)

Packer:
```lua
use {
  'nvim-tree/nvim-tree.lua',
  requires = {
    'nvim-tree/nvim-web-devicons', -- optional, for file icons
  },
}
```

Setup:
```lua
-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

require("nvim-tree").setup({
  sort_by = "modification_time",
  renderer = {
    group_empty = true,
    indent_markers = {
      enable = true
    }
  },
  diagnostics = {
    enable = true,
    show_on_dirs = true
  },
  modified = {
    enable = true
  },
  filters = {
    dotfiles = true
  },
  view = {
    width = "25%"
  },
  update_focused_file = {
    enable = true
  },
})
```

Mapping:
```lua
vim.keymap.set('n', "<leader>e", ":NvimTreeToggle<cr>", {})
```

### [toggleterm.nvim](https://github.com/akinsho/toggleterm.nvim)

Packer (commit mais novo que a tag v2.2.1 para ter o comportamento de
abrir/ocultar todos os terminais de uma só vez):
```lua
use {"akinsho/toggleterm.nvim", commit = 'bfb7a7254b5d897a5b889484c6a5142951a18b29', config = function()
  require("toggleterm").setup {
    open_mapping = [[<c-\>]],
    size = 25,
    hide_numbers = false
  }
end}
```

Keymappings:
```lua
vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], {})
vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], {})
vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], {})
vim.keymap.set('t', '<C-k>', [[<Cmd>wincmd k<CR>]], {})
vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], {})
vim.keymap.set('t', '<C-w>', [[<C-\><C-n><C-w>]], {})
vim.keymap.set('t', '<C-Up>', [[<Cmd>resize -2<CR>]], {})
vim.keymap.set('t', '<C-Down>', [[<Cmd>resize +2<CR>]], {})
vim.keymap.set('t', '<C-Left>', [[<Cmd>vertical resize -2<CR>]], {})
vim.keymap.set('t', '<C-Right>', [[<Cmd>vertical resize +2<CR>]], {})
```

### [nvim-autopairs](https://github.com/windwp/nvim-autopairs)

Autocompleta pares de '{', '(', '[', etc.

Instalado via Packer:
```lua
  use {"windwp/nvim-autopairs", config = function()
    require("nvim-autopairs").setup {
      check_ts = true,
      ts_config = {
        lua = { "string", "source" },
        javascript = { "string", "template_string" },
        java = false,
      }
    }
  end}
```

### [vim-illuminate](https://github.com/RRethy/vim-illuminate)

Packer:
```lua
  use {"RRethy/vim-illuminate",
    config = function ()
      require('illuminate').configure({
        min_count_to_highlight = 2
      })
    end
  }
```

### [indent-blankline.nvim](https://github.com/lukas-reineke/indent-blankline.nvim)

Packer:
```lua
  use {"lukas-reineke/indent-blankline.nvim",
    config = function()
      require("indent_blankline").setup {
          show_current_context = true,
          use_treesitter = true,
          show_trailing_blankline_indent = false,
      }
    end
  }
```

### [lualine](https://github.com/nvim-lualine/lualine.nvim)

Uma statusline mais bonita. Ver [arquivo de
configuração](./lua/my-nvim/lualine.lua).

### [bufferline](https://github.com/akinsho/bufferline.nvim)

Mostra uma linha com os buffers abertos, no formato de abas. Ver [arquivo de
configuração](./lua/my-nvim/bufferline.lua).

### [Trouble](https://github.com/folke/trouble.nvim)

Plugin que facilita a visualização e navegação pelos erros e TODOs do projeto.

Packer:
```lua
  use {
    "folke/trouble.nvim",
    requires = "nvim-tree/nvim-web-devicons",
    config = function()
      require("trouble").setup {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      }
    end
  }
```

Ver [arquivo de configuração](./lua/my-nvim/trouble.lua).

### [todo-comments.nvim](https://github.com/folke/todo-comments.nvim)

Melhora a visualização de comentários de TODO.

```lua
  -- Todo Comments
  use {
    "folke/todo-comments.nvim",
    requires = "nvim-lua/plenary.nvim",
    config = function()
      require("todo-comments").setup {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      }
    end
  }

```


## Linguagens

Configurações para suporte a linguagens de programação e formatos de arquivo
específicos.

Ver [diretório de configurações de linguagens](./lua/my-nvim/lsp/settings/).

### Programação

Configurações para suporte a linguagens de programação.

Para todas as linguagens de programação, eu quero as seguintes features:
  * Goto:
    + definition;
    + declaration(s);
    + references;
  * diagnostics;
    + errors;
    + warnings;
    + infos;
    + hints;
  * autocomplete;
  * treesitter syntax highlight;

#### Lua

É a linguagem usada para as configurações do NeoVim. A configuração vai
consistir em instalar um Language Server e adicionar a sua configuração ao LSP
do NeoVim. E um parser desta linguagem para o Treesitter.

##### LSP

Language Server: lua_sumneko

Pacman:
```shell
pacman -S lua-language-server
```

lspconfig:
```lua
require'lspconfig'.lua_ls.setup {
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = {'vim'},
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
        checkThirdParty = false,
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
}
```

##### Treesitter

Lua já é [suportada pelo `nvim-treesitter`](https://github.com/nvim-treesitter/nvim-treesitter#supported-languages),
então para instalar o parser de Lua basta fazer:
```
:TSInstall lua
```

#### TypeScript

Tabs com 2 espaços:

`~/.config/nvim/ftplugin/typescript.lua`:
```lua
vim.o.shiftwidth = 2
vim.o.tabstop = 2
```

##### Treesitter

```
:TSInstall typescript
:TSInstall tsx
```

##### tsserver

Instalar o language server:

```shell
npm install -g typescript typescript-language-server
```

Habilitar no NeoVim:

```lua
require'lspconfig'.tsserver.setup{
  on_attach = on_attach,
  capabilities = capabilities,
}
```

##### eslint

Instalar o language server:

```shell
npm install -g vscode-langservers-extracted eslint
```

Habilitar no NeoVim:

```lua
require'lspconfig'.eslint.setup{
  on_attach = on_attach,
  capabilities = capabilities,
  root_dir = require('lspconfig').util.find_git_ancestor,
  cmd = { home .. '/.nvm/versions/node/v16.19.0/bin/vscode-eslint-language-server', '--stdio' }
}
```

#### Java

  * Usando o plugin [nvim-jdtls](https://github.com/mfussenegger/nvim-jdtls)
    para utilizar todas as funcionalidades do Eclipse;
  * suporte ao Lombok;

No futuro talvez eu adicione suporte a testes unitário com JUnit. Mas debug,
que seria com o plugin nvim-dap, eu não tenho costume de usar, então não
pretendo instalar.

##### nvim-jdtls

Packer:

```lua
use 'mfussenegger/nvim-jdtls'
```

##### jdtls

Faça o download da sua versão favorita do jdtls: [http://download.eclipse.org/jdtls/snapshots/repository?d].
E da Lombok: [https://projectlombok.org/download].

A versão da jdtls compatível com o java-11 é a jdtls-0.52.0. Executado com este comando,
por exemplo:
```shell
java \
-Declipse.application=org.eclipse.jdt.ls.core.id1 \
-Dosgi.bundles.defaultStartLevel=4 \
-Declipse.product=org.eclipse.jdt.ls.core.product \
-Dlog.protocol=true \
-Dlog.level=ALL \
-noverify \
-Xmx2G \
--add-modules=ALL-SYSTEM \
--add-opens java.base/java.util=ALL-UNNAMED \
--add-opens java.base/java.lang=ALL-UNNAMED \
-javaagent:$HOME/.local/share/lombok/1.18.20/lombok-1.18.20.jar \
-jar $HOME/.local/share/jdtls/0.52.0/plugins/org.eclipse.equinox.launcher_1.5.700.v20200207-2156.jar \
-configuration $HOME/.local/share/jdtls/0.52.0/config_linux \
-data $HOME/workspace/$project_dirname
```

Este comando poderá ser usado em um ftplugin para iniciar o jdtls quando um arquivo `.java` for aberto.

Instalar o plugin nvim-jdtls usando Packer:
```lua
use 'mfussenegger/nvim-jdtls'
```

Alguns keybindings a mais para Java apenas:
```lua
  -- Java extensions provided by jdtls
  vim.keymap.set('n', "<C-o>", require('jdtls').organize_imports, bufopts)
  vim.keymap.set('n', "<leader>ev", require('jdtls').extract_variable, bufopts)
  vim.keymap.set('n', "<leader>ec", require('jdtls').extract_constant, bufopts)
  vim.keymap.set('v', "<leader>em", [[<ESC><CMD>lua require('jdtls').extract_method(true)<CR>]], bufopts)
```

Com esta configuração, toda vez que abrir um arquivo do tipo java, vai executar
o jdtls com a Lombok, ou se conectar a ele se já estiver em execução.
*Observe as localizações dos `.jar` e do diretório de configurações.*

```lua
-- arquivo: `~/.config/nvim/ftplugin/java.lua`:
local jdtls = require('jdtls')
local root_markers = {'gradlew', '.git'}
local root_dir = require('jdtls.setup').find_root(root_markers)
local home = os.getenv('HOME')
local workspace_folder = home .. "/.local/share/eclipse/" .. vim.fn.fnamemodify(root_dir, ":p:h:t")

local config = {
  cmd = {
    'java',
    '-Declipse.application=org.eclipse.jdt.ls.core.id1',
    '-Dosgi.bundles.defaultStartLevel=4',
    '-Declipse.product=org.eclipse.jdt.ls.core.product',
    '-Dlog.protocol=true',
    '-Dlog.level=ALL',
    '-noverify',
    '-Xmx2G',
    '--add-modules=ALL-SYSTEM',
    '--add-opens', 'java.base/java.util=ALL-UNNAMED',
    '--add-opens', 'java.base/java.lang=ALL-UNNAMED',
    '-javaagent:' .. home .. '/.local/share/lombok/1.18.20/lombok-1.18.20.jar',
    '-jar', home .. '/.local/share/jdtls/0.52.0/plugins/org.eclipse.equinox.launcher_1.5.700.v20200207-2156.jar',
    '-configuration', home .. '/.local/share/jdtls/0.52.0/config_linux',
    '-data', workspace_folder,
  },

  -- This is the default if not provided, you can remove it. Or adjust as needed.
  -- One dedicated LSP server & client will be started per unique root_dir
  root_dir = root_dir,

  -- Here you can configure eclipse.jdt.ls specific settings
  -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
  -- for a list of options
  settings = {
    java = {
    }
  },
}

-- This starts a new client & server,
-- or attaches to an existing client & server depending on the `root_dir`.
require('jdtls').start_or_attach(config)
```

##### Treesitter

```
:TSInstall java
```

#### Python

##### LSP

```lua
require'lspconfig'.pylsp.setup{
  on_attach = on_attach,
  capabilities = capabilities
}
```

##### Treesitter

```
:TSInstall python
```

#### Bash

##### Dependências

node >= 16

```sh
sudo pacman -S shellcheck bash-language-server
```

##### LSP

```lua
require'lspconfig'.bashls.setup{
  on_attach = on_attach,
  capabilities = capabilities,
  cmd = { home .. '/.nvm/versions/node/v16.19.0/bin/node', '/usr/bin/bash-language-server', 'start' }
}
```

##### Treesitter

```
:TSInstall bash
```

#### C/C++

##### LSP

Instalar dependências para o LSP clangd:
```sh
sudo pacman -S clang bear
```

Configurar o LSP clangd:
```lua
require'lspconfig'.clangd.setup{
  on_attach = on_attach,
  capabilities = capabilities
}
```

##### Treesitter

```
:TSInstall c
:TSInstall cpp
```

#### Haskell

##### LSP

[hls](https://github.com/haskell/haskell-language-server)

Pacman:
```sh
sudo pacman -S haskell-language-server
```

Packer:
```lua
require'lspconfig'.hls.setup{
  on_attach = on_attach,
  capabilities = capabilities
}
```

##### Treesitter

```
:TSInstall haskell
```

#### Javascript

##### LSP

Javascript utiliza os mesmos LSP servers que o TypeScript: tsserver e eslint.

##### Treesitter

```
:TSInstall javascript
```

### Markup

Configurações para suporte a formatos de markup.

#### Markdown

Para editar markdown, só vou precisar de syntax highlight, folds para facilitar
a navegação entre as seções e de um preview de como vai ficar em HTML o
resultado final.

##### Treesitter

Instalando a sintaxe do markdown no Treesitter já resolve as questões de syntax
highlight e folds.

```
:TSInstall markdown
:TSInstall markdown_inline
```

##### Previewer Plugin: [iamcco/markdown-preview.nvim](https://github.com/iamcco/markdown-preview.nvim)

Este plugin abre uma janela/aba do navegador padrão exibindo um preview do
markdown sendo editado conforme este é editado.

###### Packer

```lua
-- install without yarn or npm
use({
    "iamcco/markdown-preview.nvim",
    run = function() vim.fn["mkdp#util#install"]() end,
})
```

###### Configs

Só vou usar uma configuração para tentar deixar a sincronia entre o NeoVim e o
preview melhor.

```lua
-- sync_scroll_type: 'middle', 'top' or 'relative', default value is 'middle'
--   middle: mean the cursor position alway show at the middle of the preview page
--   top: mean the vim top viewport alway show at the top of the preview page
--   relative: mean the cursor position alway show at the relative positon of the preview page
vim.g.mkdp_preview_options = { sync_scroll_type = 'relative' }
```

#### ftplugin

Markdown usa tabs de dois espaços.

```
vim.o.shiftwidth = 2
vim.o.tabstop = 2
```

#### LaTeX

##### LSP

Necessário instalar os pacotes texlab e go.

```sh
sudo pacman -S texlab go
```

O [Sioyek](https://sioyek.info/) funciona melhor como previewer de documentos
LaTeX do que o Evince.

```sh
aurto add sioyek && sudo pacman -Syu sioyek
```

```lua
require'lspconfig'.texlab.setup{
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    texlab = {
      forwardSearch = {
        executable = 'sioyek',
        args = {'--reuse-window', '--inverse-search', [[nvim-texlabconfig -file %1 -line %2]], '--forward-search-file', '%f', '--forward-search-line', '%l', '%p'},
      },
    },
  },
}
```

O plugin [nvim-texlabconfig](https://github.com/f3fora/nvim-texlabconfig)
facilita a configuração do previewer. Instalado com o Packer:

```lua
use({
  'f3fora/nvim-texlabconfig',
  config = function()
      require('texlabconfig').setup()
  end,
  ft = { 'tex', 'bib' }, -- for lazy loading
  run = 'go build -o ~/bin/'
  -- run = 'go build -o ~/.bin/' if e.g. ~/.bin/ is in $PATH
})
```

##### Treesitter

```
:TSInstall latex
```

#### HTML

Tabs com 2 espaços:

`~/.config/nvim/ftplugin/html.lua`:
```lua
vim.o.shiftwidth = 2
vim.o.tabstop = 2
```

##### LSP

Instalar o language server:

```shell
npm install -g vscode-langservers-extracted
```

```lua
capabilities.textDocument.completion.completionItem.snippetSupport = true

require'lspconfig'.html.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  cmd = { home .. '/.nvm/versions/node/v16.19.0/bin/vscode-html-language-server', '--stdio' }
}
```

##### Treesitter

```
:TSInstall html
```

#### CSS

Tabs com 2 espaços:

`~/.config/nvim/ftplugin/css.lua`:
```lua
vim.o.shiftwidth = 2
vim.o.tabstop = 2
```

##### LSP

Instalar o language server:

```shell
npm install -g vscode-langservers-extracted
```

```lua
capabilities.textDocument.completion.completionItem.snippetSupport = true

require'lspconfig'.cssls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  cmd = { home .. '/.nvm/versions/node/v16.19.0/bin/vscode-css-language-server', '--stdio' }
}
```

##### Treesitter

```
:TSInstall css
```

#### JSON

Tabs com 2 espaços:

`~/.config/nvim/ftplugin/json.lua`:
```lua
vim.o.shiftwidth = 2
vim.o.tabstop = 2
```

##### LSP

Usa o language server [vscode-langservers-extracted](https://github.com/hrsh7th/vscode-langservers-extracted).

Instalar o language server:

```shell
npm install -g vscode-langservers-extracted
```

Usar o [SchemaStore](https://github.com/b0o/schemastore.nvim) para incluir uma lista popular de schemas JSON na configuração do jsonls.

Packer:
```lua
use "b0o/schemastore.nvim"
```

```lua
capabilities.textDocument.completion.completionItem.snippetSupport = true

require'lspconfig'.jsonls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  cmd = {
    home .. '/.nvm/versions/node/v16.19.0/bin/vscode-json-language-server', '--stdio'
  },
  settings = {
    json = {
      schemas = require('schemastore').json.schemas(),
      validate = { enable = true },
    },
  },
}
```

##### Treesitter

```
:TSInstall json
```

#### Yaml

##### LSP

[yamlls](https://github.com/redhat-developer/yaml-language-server)

Instalar o yaml-language-server:
```sh
pacman -S yaml-language-server
```

Configurar o yamlls:
```lua
require'lspconfig'.yamlls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
```

##### Treesitter

```
:TSInstall yaml
```

## TODO

  * [x] [nvim-autopairs](https://github.com/windwp/nvim-autopairs);
  * [x] Melhorar os Keymaps;
  * [x] [vim-illuminate](https://github.com/RRethy/vim-illuminate);
  * [x] destacar espaços no fim das linhas;
  * [x] remover git blame line do virtual text no fim da linha;
  * [x] separar e organizar configurações em módulos;
  * [x] [toggleterm](https://github.com/akinsho/toggleterm.nvim);
  * [x] adicionar navegador de arquivos em árvore (ex.:
        [nvim-tree](https://github.com/nvim-tree/nvim-tree.lua));
  * [x] [Indent Blankline indentation
        guides](https://github.com/lukas-reineke/indent-blankline.nvim);
  * [x] Criar novos splits à direita e abaixo;
  * [ ] trocar o Packer pelo [lazy.nvim](https://github.com/folke/lazy.nvim)
  * [x] [lualine](https://github.com/nvim-lualine/lualine.nvim);
  * [x] [bufferline](https://github.com/akinsho/bufferline.nvim);
  * [x] [Trouble](https://github.com/folke/trouble.nvim);
  * [x] [todo-comments.nvim](https://github.com/folke/todo-comments.nvim);
  * [x] [mason.nvim](https://github.com/williamboman/mason.nvim): "Easily install and manage LSP servers, DAP servers, linters, and formatters.";
  * [x] Resolver o problema dos diferentes caminhos e instalações do Java;
  * [ ] [lspsaga](https://github.com/glepnir/lspsaga.nvim);

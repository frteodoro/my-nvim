local M = {}

-- Retorna um plugin. Exibe mensagem padrão se não estiver instalado.
M.require_plugin = function(plugin_name)
  local status_ok, plugin = pcall(require, plugin_name)
  if status_ok then
    return plugin
  else
    vim.notify("plugin " .. plugin_name .. " não encontrado!")
    return nil
  end
end

return M

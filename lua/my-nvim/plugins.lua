-- Automatically install packer
local install_path = vim.fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = vim.fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local packer = require("my-nvim.util").require_plugin("packer")
if not packer then
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- Packer plugin manager
return packer.startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- LSP
  use 'neovim/nvim-lspconfig'
  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'

  -- Treesitter configs
  use {"nvim-treesitter/nvim-treesitter",
    -- `run = ':TSUpdate'` causa um problema na primeira instalação, por isso
    -- `nvim-treesitter.install.update()` é chamada diretamente. A função
    -- `nvim-treesitter.install.update()` retorna uma função que fará o mesmo
    -- que ':TSUpdate' ou ':TSUpdateSync', neste caso. Após a instalação
    -- ou atualização do nvim-treesitter o Packer chamará esta função que foi
    -- retornada.
    -- 
    -- Refs.:
    --  https://github.com/nvim-treesitter/nvim-treesitter/wiki/Installation#packernvim
    --  https://github.com/nvim-treesitter/nvim-treesitter/issues/3135
    run = require("nvim-treesitter.install").update({ with_sync = true })
  }

  -- devicons
  use "nvim-tree/nvim-web-devicons"

  -- Telescope
  use {"nvim-telescope/telescope.nvim",
    --tag = "0.1.3",
    -- If `ensure_dependencies` is true, the plugins specified in `requires` will
    -- be installed.
    requires = {
      {'nvim-lua/plenary.nvim'},
      {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
    }
  }

  use "folke/which-key.nvim"
  use "lewis6991/gitsigns.nvim"

  -- cmp plugins
  use "hrsh7th/nvim-cmp"          -- The completion plugin
  use "hrsh7th/cmp-buffer"        -- buffer completions
  use "hrsh7th/cmp-path"          -- path completions
  use "hrsh7th/cmp-cmdline"       -- cmdline completions
  use "hrsh7th/cmp-nvim-lsp"      -- LSP completions
  use "hrsh7th/cmp-nvim-lua"      -- completions for neovim Lua API
  use "saadparwaiz1/cmp_luasnip"  -- snippet completions

  -- snippets
  use "L3MON4D3/LuaSnip"              -- snippet engine
  use "rafamadriz/friendly-snippets"  -- a bunch of snippets to use

  -- jdtls
  use 'mfussenegger/nvim-jdtls'

  --use { 'bluz71/vim-moonfly-colors', branch = 'cterm-compat' }
  use 'bluz71/vim-moonfly-colors'

  -- markdown
  use {"iamcco/markdown-preview.nvim",
    -- install without yarn or npm
    run = function() vim.fn["mkdp#util#install"]() end,
  }

  -- LaTeX
  use {"f3fora/nvim-texlabconfig",
    config = function() require('texlabconfig').setup() end,
    ft = { 'tex', 'bib' }, -- for lazy loading
    run = 'go build -o ~/bin/'
    -- run = 'go build -o ~/.bin/' if e.g. ~/.bin/ is in $PATH
  }

  -- nvim-tree
  use {"nvim-tree/nvim-tree.lua", tag="v0.99",
    requires = {
      'nvim-tree/nvim-web-devicons', -- optional, for file icons
    },
  }

  -- JSON Schemas
  use "b0o/schemastore.nvim"

  -- toggleterm
  -- (commit mais novo que a tag v2.2.1 para ter o comportamento de
  -- abrir/ocultar todos os terminais de uma só vez)
  use {"akinsho/toggleterm.nvim", commit = 'bfb7a7254b5d897a5b889484c6a5142951a18b29'}

  -- nvim-autopairs
  use "windwp/nvim-autopairs"

  -- vim-illuminate
  use "RRethy/vim-illuminate"

  use "lukas-reineke/indent-blankline.nvim"

  -- lualine
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'nvim-tree/nvim-web-devicons', opt = true }
  }

  -- bufferline
  use {"akinsho/bufferline.nvim",
    requires = "nvim-tree/nvim-web-devicons"
  }

  -- Trouble
  use {
    "folke/trouble.nvim",
    requires = "nvim-tree/nvim-web-devicons",
  }

  -- nvim-bufdel
  use {'ojroques/nvim-bufdel'}

  -- Todo Comments
  use {
    "folke/todo-comments.nvim",
    requires = "nvim-lua/plenary.nvim",
    config = function()
      require("todo-comments").setup {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      }
    end
  }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)

-- moonfly colorscheme configs

-- O moonfly precisa destas variáveis globais configuradas antes de ser
-- carregado, senão elas não tem efeito.
--vim.g.moonflyTransparent = true
vim.g.moonflyWinSeparator = 2
vim.g.moonflyNormalFloat = true
vim.g.moonflyVirtualTextColor = true

local colorscheme = "moonfly"

local status_ok, _ = pcall(vim.cmd.colorscheme, colorscheme)
if not status_ok then
  vim.notify("colorscheme" .. colorscheme .. "não encontrado!")
  return
end

-- change the highlight style
vim.api.nvim_set_hl(0, "IlluminatedWordText", { bg = "#323266" })
vim.api.nvim_set_hl(0, "IlluminatedWordRead", { link = "IlluminatedWordText" })
vim.api.nvim_set_hl(0, "IlluminatedWordWrite", { link = "IlluminatedWordText" })

local wk = require("my-nvim.util").require_plugin("which-key")
if not wk then
  return
end

local setup = {
  notify = false,
  win = {
    border = "rounded", -- none, single, double, shadow
  },
}

local opts = {
  mode    = "n", -- NORMAL mode
  prefix  = "<leader>",
  buffer  = nil,    -- Global mappings. Specify a buffer number for buffer local mappings
  silent  = true,   -- use `silent` when creating keymaps
  noremap = true,   -- use `noremap` when creating keymaps
  nowait  = true,   -- use `nowait` when creating keymaps
}

local mappings = {
  ["e"] = { "<cmd>NvimTreeToggle<cr>", "Explorer" },

  -- Gitsigns
  g = {
    name = "Git",
    h = {
      name = "hunk",
      j = { "<cmd>lua require 'gitsigns'.next_hunk()<cr>", "Next hunk" },
      k = { "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", "Prev hunk" },
      s = { "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", "Stage hunk" },
      u = { "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>", "Undo stage hunk" },
      r = { "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", "Reset hunk" },
      v = { "<cmd>lua require 'gitsigns'.select_hunk()<cr>", "Select hunk" },
      p = { "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", "Preview hunk" },
    },
    b = {
      name = "buffer",
      s = { "<cmd>lua require 'gitsigns'.stage_buffer()<cr>", "Stage buffer" },
      u = { "<cmd>lua require 'gitsigns'.reset_buffer_index()<cr>", "Undo stage buffer" },
      r = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset buffer" },
      d = { "<cmd>Gitsigns diffthis<cr>", "Diff buffer" },
    },
    l = {
      name = "line",
      b = { "<cmd>lua require 'gitsigns'.blame_line()<cr>", "Blame line" },
    },
  },


  -- Telescope
  t = {
    name = "Telescope",
    g = { "<cmd>Telescope live_grep<cr>", "Live grep" },
    f = { "<cmd>Telescope find_files<cr>", "Find files" },
    b = { "<cmd>Telescope buffers<cr>", "Find buffer" },
    d = { "<cmd>Telescope diagnostics<cr>", "Diagnostics" },
    h = { "<cmd>Telescope help_tags<cr>", "Find help" },
    m = { "<cmd>Telescope man_pages<cr>", "Man pages" },
    r = { "<cmd>Telescope resume<cr>", "Resume last search" },
    G = {
      name = "git",
      b = { "<cmd>Telescope git_bcommits<cr>", "Buffer commits" },
      c = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
      o = { "<cmd>Telescope git_status<cr>", "Open changed file" },
      s = { "<cmd>Telescope git_stash<cr>", "Show stash" },
      B = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
    },
    T = { "<cmd>TodoTelescope<cr>", "TODOs" },
  },

  -- LSP
  l = {
    name = "LSP",
    i = { "<cmd>LspInfo<cr>", "Info" },
    g = { "goto" },
    e = { "extract" },
  },

  -- Diagnostics
  d = {
    name = "Diagnostics",
    f = { "<cmd>lua vim.diagnostic.open_float()<cr>", "Diagnostics in a floating window" },
    q = { "<cmd>lua vim.diagnostic.setqflist()<cr>", "Diagnostics to the quickfix list" },
  },

  -- Trouble
  x = {
    name = "Trouble",
    x = { "<cmd>Trouble diagnostics toggle<cr>", "Diagnostics" },
    d = { "<cmd>Trouble diagnostics toggle filter.buf=0<cr>", "Buffer Diagnostics" },
    l = { "<cmd>Trouble loclist toggle<cr>", "Location List" },
    q = { "<cmd>Trouble qflist toggle<cr>", "Quickfix List" },
    s = { "<cmd>Trouble symbols toggle focus=false<cr>", "Symbols" },
    L = { "<cmd>Trouble lsp toggle focus=false win.position=right<cr>", "LSP Definitions / references / ..." },
    T = { "<cmd>TodoTrouble<cr>", "TODOs" },
  },

  -- Buffers
  b = {
    name = "Buffers",
    d = { "<cmd>BufDel<cr>", "Delete current buffer" },
    D = { "<cmd>BufferLineCloseOthers<cr>", "Delete other buffers" }
  },

  -- Todo Comments
  T = {
    name = "Todo Comments",
    q = { "<cmd>TodoQuickFix<cr>", "Show TODOs in quickfix list" },
    x = { "<cmd>TodoTrouble<cr>", "Show TODOs in Trouble list" },
  },
}

wk.setup(setup)
wk.register(mappings, opts)

local bufferline = require("my-nvim.util").require_plugin("bufferline")
if not bufferline then
  return
end

bufferline.setup({
  options = {
    diagnostics = "nvim_lsp",
    diagnostics_indicator = function(_, level, diagnostics_dict, _)
      local icons = {
        hint = " ",
        info = " ",
        warning = " ",
        error = " ",
      }
      for e, n in pairs(diagnostics_dict) do
        --local sym = e == "error" and " "
        --  or (e == "warning" and " " or "" )
        --s = " " .. n .. sym .. count .. level
        if level:match(e) then
          local icon = icons[e]
          return " " .. n .. icon
        end
      end
    end,
    offsets = {
      {
        filetype = "NvimTree",
        text = "NvimTree",
        text_align = "center",
        separator = true
      }
    },
    custom_filter = function (buf, _)
      return not (vim.bo[buf].filetype == "qf")
    end,
  },
})



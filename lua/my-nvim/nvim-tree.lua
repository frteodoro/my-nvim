local nvim_tree = require("my-nvim.util").require_plugin("nvim-tree")
if not nvim_tree then
  return
end

local nvim_web_devicons =require("my-nvim.util").require_plugin("nvim-web-devicons")
if not nvim_web_devicons then
  return
end

nvim_web_devicons.setup({
  color_icons = true,
  default = true,
})

nvim_tree.setup({
  disable_netrw = true,
  sort_by = "case_sensitive",
  renderer = {
    group_empty = true,
    indent_markers = {
      enable = true
    }
  },
  diagnostics = {
    enable = true,
    show_on_dirs = true
  },
  modified = {
    enable = true
  },
  filters = {
    dotfiles = true
  },
  view = {
    width = "20%"
  },
  update_focused_file = {
    enable = true
  },
})

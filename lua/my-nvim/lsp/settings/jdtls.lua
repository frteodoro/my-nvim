local jdtls = require("my-nvim.util").require_plugin("jdtls")
if not jdtls then
  return
end

local function jdtls_setup(_)
  local config = {
    cmd = {
        vim.fn.expand'$HOME/.local/share/nvim/mason/bin/jdtls',
        ('--jvm-arg=-javaagent:%s'):format(vim.fn.expand'$HOME/.local/share/nvim/mason/packages/jdtls/lombok.jar')
    },

    -- Here you can configure eclipse.jdt.ls specific settings
    -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
    -- for a list of options
    settings = {
      java = {
      }
    },
    on_attach = require("my-nvim.lsp.handlers").on_attach,
    capabilities = require("my-nvim.lsp.handlers").capabilities,
  }

  -- This starts a new client & server,
  -- or attaches to an existing client & server depending on the `root_dir`.
  jdtls.start_or_attach(config)
end

-- equivalente a botar um script java.lua no diretório ftplugin
vim.api.nvim_create_autocmd('FileType', {
  pattern = {'java'},
  desc = 'Setup jdtls',
  callback = jdtls_setup,
})

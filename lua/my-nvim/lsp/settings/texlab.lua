-- LaTeX
return {
  settings = {
    texlab = {
      forwardSearch = {
        executable = 'sioyek',
        args = { '--reuse-window', '--inverse-search', [[nvim-texlabconfig -file %1 -line %2]], '--forward-search-file',
          '%f', '--forward-search-line', '%l', '%p' },
      },
    },
  },
}


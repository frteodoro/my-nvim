-- Sem o plugin lspconfig não tem o que configurar.
if not require("my-nvim.util").require_plugin("lspconfig") then
  return
end

require "my-nvim.lsp.mason"
require("my-nvim.lsp.handlers").setup()

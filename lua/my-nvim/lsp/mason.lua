local util = require("my-nvim.util")

-- Primeiro testa se estão instalados os plugins necessários.
-- Se algum não estiver instalado, avisa que está faltando e não faz nada.
local mason = util.require_plugin("mason")
local mason_lsp_config = util.require_plugin("mason-lspconfig")
local lspconfig = util.require_plugin("lspconfig")
if not (mason and mason_lsp_config and lspconfig) then
  return
end

-- LSP servers instalados automaticamente e configurados pelo lspconfig.
local servers = {
  "lua_ls",
  "ts_ls", "eslint",      -- TypeScript
  "pylsp",                -- Python
  "bashls",
  "clangd",
  "hls",                  -- Haskell
  "texlab",               -- LaTeX
  "html",
  "cssls",
  "jsonls",
  "yamlls",
  "jdtls",                -- Java
}

mason.setup()
mason_lsp_config.setup({
  ensure_installed = servers,
  automatic_installation = true,
  handlers = {
    function (server_name)
      opts = {
        on_attach = require("my-nvim.lsp.handlers").on_attach,
        capabilities = require("my-nvim.lsp.handlers").capabilities,
      }

      local require_ok, conf_opts = pcall(require, "my-nvim.lsp.settings." .. server_name)
      if require_ok then
        opts = vim.tbl_deep_extend("force", conf_opts, opts)
      end

      lspconfig[server_name].setup(opts)
    end,
    ["jdtls"] = function ()
      require("my-nvim.lsp.settings.jdtls")
    end,
  },
})

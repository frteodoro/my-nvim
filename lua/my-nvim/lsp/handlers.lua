local M = {}

-- Add additional capabilities supported by nvim-cmp
local cmp_nvim_lsp = require("my-nvim.util").require_plugin("cmp_nvim_lsp")
if not cmp_nvim_lsp then
  return
end
M.capabilities = cmp_nvim_lsp.default_capabilities()
M.capabilities.textDocument.completion.completionItem.snippetSupport = true

M.setup = function ()
  local signs = { Error = "", Warn = "", Hint = "", Info = "" }
  for type, icon in pairs(signs) do
    local hl = "DiagnosticSign" .. type
    vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
  end
end

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
M.on_attach = function(_, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_set_option_value('omnifunc', 'v:lua.vim.lsp.omnifunc', { buf = bufnr})

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap = true, silent = true, buffer = bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, vim.tbl_extend("force", bufopts, {desc="Goto declaration"}))
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, vim.tbl_extend("force", bufopts, {desc="Goto definition"}))
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, vim.tbl_extend("force", bufopts, {desc="Display hover info in a floating window"}))
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, vim.tbl_extend("force", bufopts, {desc="Goto implementation"}))
  vim.keymap.set('n', 'gr', "<cmd>Trouble lsp_references toggle focus=true<cr>", vim.tbl_extend("force", bufopts, {desc="Goto references"}))
  vim.keymap.set('n', '<leader>lgD', vim.lsp.buf.declaration, vim.tbl_extend("force", bufopts, {desc="Goto declaration"}))
  vim.keymap.set('n', '<leader>lgd', vim.lsp.buf.definition, vim.tbl_extend("force", bufopts, {desc="Goto definition"}))
  vim.keymap.set('n', '<leader>lgi', vim.lsp.buf.implementation, vim.tbl_extend("force", bufopts, {desc="Goto implementation"}))
  vim.keymap.set('n', '<leader>lgr', "<cmd>Trouble lsp_references toggle focus=true<cr>", vim.tbl_extend("force", bufopts, {desc="Goto references"}))
  vim.keymap.set('n', '<leader>la', vim.lsp.buf.code_action, vim.tbl_extend("force", bufopts, {desc="Code action"}))
  vim.keymap.set('n', '<leader>lf', function() vim.lsp.buf.format { async = true } end, vim.tbl_extend("force", bufopts, {desc="Format"}))
  vim.keymap.set('n', '<leader>lr', vim.lsp.buf.rename, vim.tbl_extend("force", bufopts, {desc="Rename"}))
  vim.keymap.set('n', '<leader>ls', vim.lsp.buf.signature_help, vim.tbl_extend("force", bufopts, {desc="Signature help"}))

  -- Java extensions provided by jdtls
  vim.keymap.set('n', "<leader>lo", require('jdtls').organize_imports, vim.tbl_extend("force", bufopts, {desc="Organize import"}))
  vim.keymap.set('n', "<leader>lev", require('jdtls').extract_variable, vim.tbl_extend("force", bufopts, {desc="Extract variable"}))
  vim.keymap.set('n', "<leader>lec", require('jdtls').extract_constant, vim.tbl_extend("force", bufopts, {desc="Extract constant"}))
  vim.keymap.set('v', "<leader>lem", [[<ESC><CMD>lua require('jdtls').extract_method(true)<CR>]], vim.tbl_extend("force", bufopts, {desc="Extract method"}))
end

return M

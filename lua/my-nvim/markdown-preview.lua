-- markdown-preview settings
-- https://github.com/iamcco/markdown-preview.nvim#markdownpreview-config
vim.g.mkdp_preview_options = { sync_scroll_type = 'relative' }

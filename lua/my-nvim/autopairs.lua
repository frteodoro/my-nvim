local autopairs = require("my-nvim.util").require_plugin("nvim-autopairs")
if (not autopairs) then
  return
end

autopairs.setup {
  check_ts = true,
  ts_config = {
    lua = { "string", "source" },
    javascript = { "string", "template_string" },
    java = false,
  },
  fast_wrap = {
    manual_position = false,
  },
}

-- insert `(` after select function or method item
local cmp = require("my-nvim.util").require_plugin("cmp")
if cmp then
  local cmp_autopairs = require('nvim-autopairs.completion.cmp')
  cmp.event:on(
    'confirm_done',
    cmp_autopairs.on_confirm_done({ map_char = { tex = "" } })
  )
end

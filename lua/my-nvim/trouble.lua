local trouble = require('my-nvim.util').require_plugin('trouble')
if not trouble then
  return
end

trouble.setup()

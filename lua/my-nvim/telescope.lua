local telescope = require("my-nvim.util").require_plugin("telescope")
if not telescope then
  return
end

telescope.setup({
  defaults = {
    --path_display = {shorten = {exclude = {-3, -2, -1}}},
    path_display = {"smart"},
    color_devicons = true,
  },
})

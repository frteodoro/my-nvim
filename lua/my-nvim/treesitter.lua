local ts_configs = require("my-nvim.util").require_plugin("nvim-treesitter.configs")
if not ts_configs then
  return
end

ts_configs.setup({
  highlight = {
    -- `false` will disable the whole extension
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gnn", -- set to `false` to disable one of the mappings
      node_incremental = "grn",
      scope_incremental = "grc",
      node_decremental = "grm",
    },
  },
  indent = {
    enable = true
  },
  autopairs = {
    enable = true,
  },
})

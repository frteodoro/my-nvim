local toggleterm = require("my-nvim.util").require_plugin("toggleterm")
if not toggleterm then
  return
end

toggleterm.setup {
  open_mapping = [[<c-\>]],
  size = 25,
}

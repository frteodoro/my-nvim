local opts = { noremap=true, silent=true }

-- tecla <leader>
vim.g.mapleader = ' '

-- Better window movement
vim.keymap.set('n', '<C-h>', '<C-w>h', vim.tbl_extend("force", opts, {desc="Go to the left window"}))
vim.keymap.set('n', '<C-j>', '<C-w>j', vim.tbl_extend("force", opts, {desc="Go to the down window"}))
vim.keymap.set('n', '<C-k>', '<C-w>k', vim.tbl_extend("force", opts, {desc="Go to the up window"}))
vim.keymap.set('n', '<C-l>', '<C-w>l', vim.tbl_extend("force", opts, {desc="Go to the right window"}))

-- Resize with arrows
vim.keymap.set('n', '<C-Up>', ':resize -2<CR>', vim.tbl_extend("force", opts, {desc="Decrease height"}))
vim.keymap.set('n', '<C-Down>', ':resize +2<CR>', vim.tbl_extend("force", opts, {desc="Increase height"}))
vim.keymap.set('n', '<C-Left>', ':vertical resize -2<CR>', vim.tbl_extend("force", opts, {desc="Decrease width"}))
vim.keymap.set('n', '<C-Right>', ':vertical resize +2<CR>', vim.tbl_extend("force", opts, {desc="Increase width"}))

-- Navigate buffers
vim.keymap.set("n", "<S-l>", ":bnext<CR>", vim.tbl_extend("force", opts, {desc="Next buffer"}))
vim.keymap.set("n", "<S-h>", ":bprevious<CR>", vim.tbl_extend("force", opts, {desc="Prev buffer"}))

-- QuickFix
function QuickFixToggle()
    for _, win in pairs(vim.fn.getwininfo()) do
        if win["quickfix"] == 1 then
            vim.cmd "cclose"
            return
        end
    end
    vim.cmd "copen"
end
vim.keymap.set('n', ']q', ':cnext<CR>', vim.tbl_extend("force", opts, {desc="Next quickfix"}))
vim.keymap.set('n', '[q', ':cprev<CR>', vim.tbl_extend("force", opts, {desc="Prev quickfix"}))
vim.keymap.set('n', '<C-q>', ':lua QuickFixToggle()<CR>', vim.tbl_extend("force", opts, {desc="Toggle quickfix window"}))

-- Better indenting
vim.keymap.set('v', '<', '<gv', vim.tbl_extend("force", opts, {desc="Increase indentation"}))
vim.keymap.set('v', '>', '>gv', vim.tbl_extend("force", opts, {desc="Decrease indentation"}))

-- Diagnostic
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, vim.tbl_extend("force", opts, {desc="Next diagnostic"}))
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, vim.tbl_extend("force", opts, {desc="Prev diagnostic"}))

-- Gitsigns
vim.keymap.set('n', ']h', vim.diagnostic.goto_next, vim.tbl_extend("force", opts, {desc="Next diagnostic"}))
vim.keymap.set('n', '[h', vim.diagnostic.goto_prev, vim.tbl_extend("force", opts, {desc="Prev diagnostic"}))

-- toggleterm
vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], vim.tbl_extend("force", opts, {desc="Normal mode"}))
vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], vim.tbl_extend("force", opts, {desc="Go to the left window"}))
vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], vim.tbl_extend("force", opts, {desc="Go to the down window"}))
vim.keymap.set('t', '<C-k>', [[<Cmd>wincmd k<CR>]], vim.tbl_extend("force", opts, {desc="Go to the up window"}))
vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], vim.tbl_extend("force", opts, {desc="Go to the right window"}))
vim.keymap.set('t', '<C-w>', [[<C-\><C-n><C-w>]], vim.tbl_extend("force", opts, {desc="Window options"}))
vim.keymap.set('t', '<C-Up>', [[<Cmd>resize -2<CR>]], vim.tbl_extend("force", opts, {desc="Decrease height"}))
vim.keymap.set('t', '<C-Down>', [[<Cmd>resize +2<CR>]], vim.tbl_extend("force", opts, {desc="Increase height"}))
vim.keymap.set('t', '<C-Left>', [[<Cmd>vertical resize -2<CR>]], vim.tbl_extend("force", opts, {desc="Decrease width"}))
vim.keymap.set('t', '<C-Right>', [[<Cmd>vertical resize +2<CR>]], vim.tbl_extend("force", opts, {desc="Increase width"}))

-- Todo Comments
local todoComments = require("my-nvim.util").require_plugin("todo-comments")
if todoComments then
  -- Keymaps
  vim.keymap.set("n", "]t", function() todoComments.jump_next() end, { desc = "Next todo comment" })
  vim.keymap.set("n", "[t", function() todoComments.jump_prev() end, { desc = "Previous todo comment" })
end

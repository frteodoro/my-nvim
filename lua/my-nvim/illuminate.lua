local illuminate = require("my-nvim.util").require_plugin("illuminate")
if not illuminate then
  return
end

illuminate.configure({
  filetypes_denylist = {
    "NvimTree",
  },
  min_count_to_highlight = 2,
})

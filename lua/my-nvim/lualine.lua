local lualine = require("my-nvim.util").require_plugin("lualine")
if not lualine then
  return
end

lualine.setup({
  sections = {
    lualine_x = {'encoding', 'fileformat', {'filetype', icon_only = true}},
  },
  extensions = {"fzf", "man", "nvim-tree", "quickfix", "toggleterm"}
})

-- :help options

-- Speed up loading Lua modules in Neovim to improve startup time.
vim.loader.enable()

-- nvim-tree
-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

vim.opt.termguicolors = true

-- yank to clipboard
vim.opt.clipboard = "unnamedplus"

-- highlight yanked text
vim.api.nvim_create_autocmd('TextYankPost', {
  group = vim.api.nvim_create_augroup('highlight_yank', {}),
  desc = 'Hightlight selection on yank',
  pattern = '*',
  callback = function()
    vim.highlight.on_yank({higroup = 'IncSearch', timeout = 200})
  end,
})

-- highlight extra spaces
vim.api.nvim_set_hl(0, 'ExtraWhiteSpace', {ctermbg='Red', bg='Red'})
vim.fn.matchadd('ExtraWhiteSpace', [[\s\+\%#\@<!$]])

-- indentação
vim.opt.shiftwidth = 4   	-- indentação tem 4 espaços
vim.opt.tabstop = 4      	-- tab tem 4 espaços
vim.opt.expandtab = true 	-- converte tabs para espaços

-- Destaque para a linha corrente
vim.opt.cursorline = true

-- Números de Linhas
vim.opt.number = true
vim.opt.relativenumber = true

-- Número mínimo de linhas exibidas acima e abaixo do cursor
vim.opt.scrolloff = 3

-- milissegundos antes de apresentar o which-key
vim.opt.timeoutlen = 200

-- always show the sign column, otherwise it would shift the text each time
vim.opt.signcolumn = "yes"

-- new splits config
vim.opt.splitbelow = true
vim.opt.splitright = true

-- folding options
vim.opt.foldenable = true                           -- enable folding at startup.
vim.opt.foldlevel = 6
vim.o.foldmethod = "expr"                         -- for treesitter based folding
vim.o.foldexpr = "nvim_treesitter#foldexpr()"     -- for treesitter based folding

-- more space in the neovim command line for displaying messages
vim.opt.cmdheight = 2

-- make options
vim.cmd("set errorformat^=%*[^:]:\\ %f:%l:\\ %m")   -- match libc assert

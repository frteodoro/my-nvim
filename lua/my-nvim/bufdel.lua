local bufdel = require("my-nvim.util").require_plugin("bufdel")
if not bufdel then
  return
end

bufdel.setup({
  next = 'alternate',
  quit = true,  -- quit Neovim when last buffer is closed
})

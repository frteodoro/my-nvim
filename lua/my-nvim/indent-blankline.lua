local ibl = require("my-nvim.util").require_plugin("ibl")
if not ibl then
  return
end

ibl.setup({
  indent = {
    char = "▏",
  }
})

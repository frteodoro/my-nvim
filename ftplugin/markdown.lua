-- :help ft-markdown-plugin
vim.g.markdown_folding = true
vim.g.markdown_recommended_style = false

vim.o.shiftwidth = 2
vim.o.tabstop = 2
